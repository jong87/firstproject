package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class Type {
	private String uid;
	private String name;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Type [uid=" + uid + ", name=" + name + "]";
	} 
	
	public void compile() {
		System.out.println("compile");
	}
	
}
